package com.web.demo.controls;

import com.web.demo.models.Transaction;
import com.web.demo.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.transaction.TransactionTimedOutException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/accounts/{accountNumber}/transactions")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public List<Transaction> getAllTransactions(@PathVariable Long accountNumber) {
        return transactionService.getAllTransactions();
    }

    @GetMapping("/{id}")
    public Transaction getTransactionById(@PathVariable Long accountNumber, @PathVariable Long id) {
        return transactionService.getTransactionById(id);
    }

    @PostMapping
    public Transaction createTransaction(@PathVariable Long accountNumber, @RequestBody Transaction transaction) {
        return transactionService.saveTransaction(accountNumber, transaction);
    }

    @PostMapping("lock")
    public Transaction createTransactionWithLock(@PathVariable Long accountNumber, @RequestBody Transaction transaction) {
        try {
            return transactionService.saveTransactionWithLock(accountNumber, transaction);
        } catch (JpaSystemException e) {
            if (e.getCause() instanceof TransactionTimedOutException) {
                throw new TransactionTimedOutException("Transaction timed out, please try again later");
            } else {
                throw e;
            }
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred while processing the deposit");
        }
    }

    @DeleteMapping("/{id}")
    public void deleteTransaction(@PathVariable Long accountNumber, @PathVariable Long id) {
        transactionService.deleteTransaction(id);
    }
}
