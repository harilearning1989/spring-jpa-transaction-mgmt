package com.web.demo.services;

import com.web.demo.models.Account;

import java.util.List;
import java.util.Optional;

public interface AccountService {
    List<Account> getAllAccounts();
    Account getAccountById(Long id);
    Account saveAccount(Account account);
    void deleteAccount(Long id);
    Account getAccountByAccountNumber(Long id);

    Optional<Account> findByAccountForUpdate(Long accountNumber);
}
