package com.web.demo.services;

import com.web.demo.models.Transaction;

import java.util.List;

public interface TransactionService {

    List<Transaction> getAllTransactions();

    Transaction getTransactionById(Long id);

    Transaction saveTransaction(Long accountId, Transaction transaction);

    Transaction saveTransactionWithLock(Long accountId, Transaction transaction);

    void deleteTransaction(Long id);
}
