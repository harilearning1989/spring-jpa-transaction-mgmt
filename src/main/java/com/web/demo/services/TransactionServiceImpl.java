package com.web.demo.services;

import com.web.demo.models.Account;
import com.web.demo.models.Transaction;
import com.web.demo.repos.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class TransactionServiceImpl implements TransactionService{

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountService accountService;

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }

    @Override
    public Transaction getTransactionById(Long id) {
        return transactionRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional(timeout = 6)
    public Transaction saveTransactionWithLock(Long accountNumber, Transaction transaction) {
        Optional<Account> accountOpt = accountService.findByAccountForUpdate(accountNumber);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        long sleepTimeInSeconds = 5;
        try {
            timeUnit.sleep(sleepTimeInSeconds);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (accountOpt.isPresent()) {
            Account account = accountOpt.get();
            transaction.setAccount(account);
            transaction.setDateTime(LocalDateTime.now());
            double newBalance = account.getBalance() + (transaction.getType().equals("DEPOSIT") ? transaction.getAmount() : -transaction.getAmount());
            account.setBalance(newBalance);
            //accountService.saveAccount(account);
            return transactionRepository.save(transaction);
        }
        return null;
    }

    @Override
    public Transaction saveTransaction(Long accountNumber, Transaction transaction) {
        Account account = accountService.getAccountByAccountNumber(accountNumber);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        long sleepTimeInSeconds = 5;
        try {
            timeUnit.sleep(sleepTimeInSeconds);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (account != null) {
            transaction.setAccount(account);
            transaction.setDateTime(LocalDateTime.now());
            double newBalance = account.getBalance() + (transaction.getType().equals("DEPOSIT") ? transaction.getAmount() : -transaction.getAmount());
            account.setBalance(newBalance);
            //accountService.saveAccount(account);
            return transactionRepository.save(transaction);
        }
        return null;
    }

    @Override
    public void deleteTransaction(Long id) {
        transactionRepository.deleteById(id);
    }
}
