package com.web.demo.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "TRANSACTION")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String depositorName;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PHONE_NUMBER")
    private long phoneNumber;
    @Column(name = "AMOUNT")
    private double amount;
    @Column(name = "DATETIME")
    private LocalDateTime dateTime;
    @Column(name = "TYPE")
    private String type; // DEPOSIT or WITHDRAW

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;
}
