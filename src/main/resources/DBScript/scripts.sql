-- PRACTICE.EMPLOYEE_DETAILS definition

CREATE TABLE `EMPLOYEE_DETAILS` (
    `EMP_ID` int NOT NULL AUTO_INCREMENT,
    `EMP_NAME` varchar(30) DEFAULT NULL,
    `USERNAME` varchar(30) DEFAULT NULL,
    `EMAIL` varchar(30) DEFAULT NULL,
    `AGE` int DEFAULT NULL,
    `PHONE` varchar(30) DEFAULT NULL,
    `GENDER` varchar(6) DEFAULT NULL,
    `DATE_CREATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `LAST_UPDATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
    `version` bigint DEFAULT 0,
    PRIMARY KEY (`EMP_ID`)
) ;

CREATE TABLE Account (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    ACCOUNT_NUMBER VARCHAR(50) NOT NULL UNIQUE,
    ACCOUNT_HOLDER_NAME VARCHAR(255) NOT NULL,
    balance DOUBLE NOT NULL
);

--ALTER TABLE account ADD COLUMN account_number VARCHAR(50) NOT NULL UNIQUE AFTER id;
--ALTER TABLE Transaction ADD COLUMN phone_number VARCHAR(50) after email;
--alter table `Transaction` drop COLUMN phone_number;

CREATE TABLE `Transaction` (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name varchar(50) DEFAULT NULL,
    email varchar(50) DEFAULT NULL,
    phone_number varchar(50) DEFAULT NULL,
    amount DOUBLE NOT NULL,
    dateTime TIMESTAMP NOT NULL,
    type VARCHAR(50) NOT NULL,
    account_id BIGINT,
    CONSTRAINT FK_Account_Transaction FOREIGN KEY (account_id) REFERENCES Account(id) ON DELETE CASCADE
);
